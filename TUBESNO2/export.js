let { Pertandingan } = require("./index")

function main(){
    let notice = new Pertandingan("Christiano Ronaldo", "World Cup", "5 Desember 2022", "M. Salah")
    console.log("Dalam pertandingan di " + notice.event + " " + notice.nama + " bermain pada tanggal " + notice.tanggal)

    notice.lokasi("Portugal")
    notice.totalPemain(831)
}

main()