class Pemain{
    constructor(nama, nama2){
        this.nama = nama
        this.nama2 = nama2
        console.log(nama + " sedang bermain bersama " + nama2)
    }
    lokasi(negara){
        console.log(this.nama + " berasal dari " + negara)
    }
}

class Pertandingan extends Pemain{
    constructor(nama, event, tanggal, nama2){
        super(nama, nama2)    
        this.event = event
        this.tanggal = tanggal
    }
    totalPemain(jumlahPemain){
        const negara = 32
        let totalTeam = Math.trunc(jumlahPemain / negara)
        console.log("Ada " + totalTeam + " pemain yang berpartisipasi di setiap tim")
    }
}

exports.Pertandingan = Pertandingan